const express = require('express')
const authController = require('../../controller/UsersController')
const router = express.Router()

router.get('/', authController.register)

module.exports = router
