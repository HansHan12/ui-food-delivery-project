const express = require('express')
const multer = require('multer')
const md5 = require('md5')
const router = express.Router()
const InternalAPI = require('../../config/api/api')

const localStorageFood = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/LocalStorage')
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname)
  }
})

const localUploadFood = multer({ storage: localStorageFood, limits: { fieldSize: 1024 * 1024 * 10 } })

router.get('/getAllData', (req, res) => {
  const reqData = {
    method: 'GET',
    params: {}
  }
  // console.log(reqData)
  InternalAPI.getMenu(reqData, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.get('/getAllFoods', (req, res) => {
  const reqData = {
    method: 'GET',
    params: {}
  }
  // console.log(reqData)
  InternalAPI.getFood(reqData, res, function (resp) {
    console.log(resp)
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.get('/getAllDrinks', (req, res) => {
  const reqData = {
    method: 'GET',
    params: {}
  }
  // console.log(reqData)
  InternalAPI.getDrink(reqData, res, function (resp) {
    console.log(resp)
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.get('/getAllSnacks', (req, res) => {
  const reqData = {
    method: 'GET',
    params: {}
  }
  // console.log(reqData)
  InternalAPI.getSnack(reqData, res, function (resp) {
    console.log(resp)
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.post('/createFood', localUploadFood.single('image'), (req, res) => {
  // console.log(req.body)

  const itemCode = req.body.itemCode
  const pathImage = `LocalStorage/${req.file.filename}`
  if (!req.file) {
    console.log('Please Upload Image File')
    return false
  }
  const Data = {
    itemCode,
    nama: req.body.nama,
    category: req.body.category,
    qty: req.body.qty,
    price: req.body.price,
    image: pathImage
  }
  // console.log(reqData)
  InternalAPI.postData(Data, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.post('/updateMenu/', (req, res) => {
  console.log(req.body)
  const Data = {
    itemCode: req.body.itemCode,
    nama: req.body.nama,
    category: req.body.category,
    qty: req.body.qty,
    price: req.body.price
  }
  // console.log(reqData)
  InternalAPI.putData(Data, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.post('/deleteMenu', (req, res) => {
  const itemCode = req.body.itemCode
  console.log(itemCode)
  const data = {
    itemCode
  }
  InternalAPI.delete(data, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.post('/createDrink', localUploadFood.single('image'), (req, res) => {
  // console.log(req.body)
  const pathImage = `LocalStorage/${req.file.filename}`
  if (!req.file) {
    console.log('Please Upload Image File')
    return false
  }
  const Data = {
    itemCode: req.body.itemCode,
    nama: req.body.nama,
    category: req.body.category,
    qty: req.body.qty,
    price: req.body.price,
    image: pathImage
  }
  // console.log(reqData)
  InternalAPI.postDrink(Data, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.post('/createSnack', localUploadFood.single('image'), (req, res) => {
  // console.log(req.body)
  const pathImage = `LocalStorage/${req.file.filename}`
  if (!req.file) {
    console.log('Please Upload Image File')
    return false
  }
  const Data = {
    itemCode: req.body.itemCode,
    nama: req.body.nama,
    category: req.body.category,
    qty: req.body.qty,
    price: req.body.price,
    image: pathImage
  }
  // console.log(reqData)
  InternalAPI.postSnack(Data, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

router.post('/createAccount', (req, res) => {
  const Data = {
    nama: req.body.nama,
    email: req.body.email,
    password: md5(req.body.password)
  }
  // console.log(Data)
  InternalAPI.postRegister(Data, res, function (resp) {
    // console.log(resp)
    res.send(resp.data)
  })
})

router.post('/login', (req, res) => {
  const data = {
    email: req.body.email,
    password: md5(req.body.password)
  }
  // console.log(reqData)
  InternalAPI.login(data, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

module.exports = router
