const axios = require('axios')
const Authorization = 'Basic ' + Buffer.from(`hanshan:hanshan`).toString('base64')

exports.getMenu = (data, res, req, handler, fallout) => {
  axios.get('http://localhost:5000/api-product/menu', {
    headers: {
      Authorization: Authorization
    }
  })
    .then((response) => {
      // handler(response.data)
      res.send(response.data)
      // return response.data
    })
    .catch((err) => {
      res.send(err)
    })
}

exports.getFood = (data, res, req, handler, fallout) => {
  axios.get('http://localhost:5000/api-product/foods', {
    headers: {
      Authorization: Authorization
    }
  })
    .then((response) => {
      // handler(response.data)
      res.send(response.data)
      // console.log(response.data)
      // return response.data
    })
    .catch((err) => {
      res.send(err)
    })
}

exports.getDrink = (data, res, req, handler, fallout) => {
  axios.get('http://localhost:5000/api-product/drinks', {
    headers: {
      Authorization: Authorization
    }
  })
    .then((response) => {
      // handler(response.data)
      res.send(response.data)
      // console.log(response.data)
      // return response.data
    })
    .catch((err) => {
      res.send(err)
    })
}

exports.getSnack = (data, res, req, handler, fallout) => {
  axios.get('http://localhost:5000/api-product/snacks', {
    headers: {
      Authorization: Authorization
    }
  })
    .then((response) => {
      // handler(response.data)
      res.send(response.data)
      // console.log(response.data)
      // return response.data
    })
    .catch((err) => {
      res.send(err)
    })
}

exports.postData = function (data, res, next, callback, fallout) {
  console.log(data)
  axios.post('http://localhost:5000/api-product/create-menu', data, {
    headers: {
      Authorization: Authorization
    }
  })
    .then(function (resp) {
      // console.log(resp)
      if (resp.data == 'Item Code has been used, please use another Item Code') {
        res.send({ data: resp.data })
      } else {
        res.redirect('/home')
      }
    })
    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.postDrink = function (data, res, next, callback, fallout) {
  console.log(data)
  axios.post('http://localhost:5000/api-product/create-menu', data, {
    headers: {
      Authorization: Authorization
    }
  })
    .then(function (resp) {
      res.redirect('/drink')
    })
    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.postSnack = function (data, res, next, callback, fallout) {
  console.log(data)
  axios.post('http://localhost:5000/api-product/create-menu', data, {
    headers: {
      Authorization: Authorization
    }
  })
    .then(function (resp) {
      res.redirect('/snack')
    })

    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.postRegister = function (data, res, next, callback, fallout) {
  axios.post('http://localhost:5000/authentication/register', data, {
    headers: {
      Authorization: Authorization
    }
  })
    .then(function (resp) {
      res.redirect('/login/login2')
    })
    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.login = function (data, res, next, callback, fallout) {
  axios.post('http://localhost:5000/authentication/login', data, {
    headers: {
      Authorization: Authorization
    }
  })
    .then(function (resp) {
      // res.redirect('/home')
    })
    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.putData = function (data, res, next, callback, fallout) {
  // console.log(data)
  axios.put('http://localhost:5000/api-product/update-menu', data, {
    headers: {
      Authorization: Authorization
    }
  })
    .then(function (resp) {
      res.redirect('/home')
    })
    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}

exports.delete = function (data, res, next, callback, fallout) {
  // console.log(data)
  axios.delete('http://localhost:5000/api-product/delete-menu', {
    headers: {
      Authorization: Authorization
    }, data
  })
    .then(function (resp) {
      res.redirect('/home')
    })
    .catch(function (err) {
      if (fallout) {
        fallout(err)
      } else {
        next(err)
      }
    })
}
