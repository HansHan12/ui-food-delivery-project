const express = require('express')
const InternalAPI = require('../../../config/api/api')
const router = express.Router()

router.get('/', (req, res) => {
  const linkMenuDrink = req.params
  const version = require('../../../package.json')
  res.render('content/menu/drink', { title: 'Drink - Cavalese', vPackage: version, linkMenuDrink: linkMenuDrink })
})

router.get('/order', (req, res) => {
  const linkOrder = req.params
  const version = require('../../../package.json')
  res.render('content/order', { title: 'Order - Cavalese', vPackage: version, linkOrder: linkOrder })
})

router.get('/getAllDrink', (req, res) => {
  const reqData = {
    method: 'GET',
    params: {}
  }
  console.log(reqData)
  InternalAPI.getDrink(reqData, res, function (resp) {
    // console.log(resp)
    // res.send(resp.data)
  })
})

module.exports = router
