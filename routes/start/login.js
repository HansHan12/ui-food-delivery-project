const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  res.render('start/login', { title: 'Sign Up - Cavalese' })
})

router.get('/login2', (req, res) => {
  res.render('start/login2', { title: 'Sign Up - Cavalese' })
})

router.get('/login3', (req, res) => {
  res.render('start/login3', { title: 'Sign Up - Cavalese' })
})

module.exports = router
