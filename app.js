const express = require('express')
const path = require('path')
const swaggerUI = require('swagger-ui-express')
const bodyParser = require('body-parser')
const favicon = require('serve-favicon')
const apiDocumentation = require('./config/api/api-docs')
const app = express()

// view engine setup
app.set('view engine', 'pug')
app.use(express.static(path.join(__dirname, 'public')))
app.use(bodyParser.json())
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.urlencoded({ extended: true }))
const options = {
  customCss: '.swagger-ui .topbar {display: none}'
}

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(apiDocumentation, options))

// API Router
app.use('/api-product', require('./routes/product'))
app.use('/authentication', require('./routes/authentication'))

app.use('/protected', require('./routes/protected'))

// Call Router
app.use('/login', require('./routes/start/login'))
app.use('/login3', require('./routes/start/login'))
app.use('/home', require('./routes/content/home'))
app.use('/aboutUs', require('./routes/content/aboutUs'))
app.use('/api', require('./routes/content/service'))
app.use('/CS', require('./routes/content/CS'))
app.use('/order', require('./routes/content/home'))
app.use('/profile', require('./routes/content/profile'))
app.use('/transaction', require('./routes/content/transaction'))
app.use('/food', require('./routes/content/menu/food'))
app.use('/drink', require('./routes/content/menu/drink'))
app.use('/snack', require('./routes/content/menu/snack'))
app.use('/404', require('./routes/404'))

// Admin
app.use('/Ahome', require('./routes/Admin/content/home'))
app.use('/editMenu', require('./routes/Admin/content/editMenu'))
app.use('/Cmessage', require('./routes/Admin/content/Cmessage'))

app.get('/', (req, res) => {
  res.render('start/register', { title: 'Sign Up - Cavalese' })
})

app.use((err, res, next) => {
  if (err) {
    res.redirect('/404')
  }
})

app.listen(5000, function () {
  console.log('Server started on http://localhost:5000')
})

module.exports = app
