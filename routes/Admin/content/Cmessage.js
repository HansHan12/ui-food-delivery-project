const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  const linkHome = req.params
  const version = require('../../../package.json')
  res.render('Admin/content/Cmessage', { title: 'Message - Cavalese', vPackage: version, linkHome: linkHome })
})

module.exports = router
