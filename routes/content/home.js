const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  const linkHome = req.params
  const version = require('../../package.json')
  res.render('content/home', { title: 'Home - Cavalese', vPackage: version, linkHome: linkHome })
})

router.get('/order', (req, res) => {
  const version = require('../../package.json')
  const linkOrder = req.params
  res.render('content/order', { title: 'Order - Cavalese', vPackage: version, linkOrder: linkOrder })
})

module.exports = router
