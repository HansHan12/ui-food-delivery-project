const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  const linkMenuSnack = req.params
  const version = require('../../../package.json')
  res.render('content/menu/snack', { title: 'Snack - Cavalese', vPackage: version, linkMenuSnack: linkMenuSnack })
})

router.get('/order', (req, res) => {
  const linkOrder = req.params
  const version = require('../../../package.json')
  res.render('content/order', { title: 'Order - Cavalese', vPackage: version, linkOrder: linkOrder })
})

module.exports = router
