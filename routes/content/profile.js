const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  const linkProfile = req.params
  const version = require('../../package.json')
  res.render('content/profile', { title: 'Customer Service - Cavalese', vPackage: version, linkProfile: linkProfile })
})

module.exports = router
