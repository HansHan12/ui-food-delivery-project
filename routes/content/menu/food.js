const express = require('express')
const router = express.Router()

router.get('/', function (req, res) {
  const linkMenuFood = req.params
  const version = require('../../../package.json')
  res.render('content/menu/food', { title: 'Food - Cavalese', vPackage: version, linkMenuFood: linkMenuFood })
})

router.get('/order/:itemCode', (req, res) => {
  const linkOrder = req.params.itemCode
  const version = require('../../../package.json')
  res.render('content/order', { title: 'Order - Cavalese', vPackage: version, linkOrder: linkOrder })
})

module.exports = router
