const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  const linkTransaction = req.params
  const version = require('../../package.json')
  res.render('content/transaction', { title: 'transaction - Cavalese', vPackage: version, linkTransaction: linkTransaction })
})

module.exports = router
