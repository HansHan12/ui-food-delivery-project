$(document).ready(function () {
  $("#sidebar").mCustomScrollbar({
    theme: "minimal"
  });
    $('#sidebarCollapse').on('click', function () {
      $('#sidebar, #content').toggleClass('active');
      $('.collapse.in').toggleClass('in');
      $('a[aria-expanded=true]').attr('aria-expanded', 'false');
  });
});

$(document).ready(function () {
  $('.btn-all-menu').on('click', () => {
    $('#AllMenu').show()
    $('#card-food').hide()
    $('#Drink').hide()
    $('#Snack').hide()
    $('#Food').hide()
  })
})

$(document).ready(function () {
  $('.btn-food').on('click', () => {
    $('#Food').show()
    $('#card-food').show()
    $('#Drink').hide()
    $('#Snack').hide()
    $('#AllMenu').hide()
  })
})

$(document).ready(function () {
  $('.btn-drink').on('click', () => {
    $('#Drink').show()
    $('#card-food').hide()
    $('#Snack').hide()
    $('#Food').hide()
    $('#AllMenu').hide()
  })
})

$(document).ready(function () {
  $('.btn-snack').on('click', () => {
    $('#Snack').show()
    $('#card-food').hide()
    $('#Drink').hide()
    $('#Food').hide()
    $('#AllMenu').hide()
  })
})

document.addEventListener("DOMContentLoaded", function(){
  window.addEventListener('scroll', function() {
      if (window.scrollY > 50) {
        document.getElementById('navbar_top').classList.add('fixed-top');
        // add padding top to show content behind navbar
        navbar_height = document.querySelector('.navbar').offsetHeight;
        document.body.style.paddingTop = navbar_height + 'px';
      } else {
        document.getElementById('navbar_top').classList.remove('fixed-top');
        // remove padding top from body
        document.body.style.paddingTop = '0';
      } 
  });
});

// modal
$(document).ready(function () {
	$('#choose-file').change(function () {
		var i = $(this).prev('label').clone();
		var file = $('#choose-file')[0].files[0].name;
		$(this).prev('label').text(file);
	}); 
 });

$('#exampleModal').on('hidden.bs.modal', function () {
  $(this).find('form').trigger('reset');
})

setTimeout(
  function() {
    var loader = document.getElementById("loader");
    loader.style.display = 'none'
  }, 1000
);