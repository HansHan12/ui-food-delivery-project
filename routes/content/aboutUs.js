const express = require('express')
const router = express.Router()

router.get('/', (req, res) => {
  const linkAboutUs = req.params
  const version = require('../../package.json')
  res.render('content/aboutUs', { title: 'About Us - Cavalese', vPackage: version, linkAboutUs: linkAboutUs })
})

module.exports = router
